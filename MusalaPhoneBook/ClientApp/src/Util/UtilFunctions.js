import _ from "lodash";

export class UtilFunctions {
    static generateRandomString(length){
        let chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        length = length > chars.length?chars.length:length;
        return _.shuffle(chars).join("").substring(0, length);
    }
}