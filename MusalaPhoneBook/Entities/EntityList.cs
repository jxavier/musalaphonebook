using System.Collections.Generic;

namespace MusalaPhoneBook.Entities
{
    public class EntityList<T>
    {
        public long Total { get; set; }
        public IEnumerable<T> List { get; set; }
    }
}