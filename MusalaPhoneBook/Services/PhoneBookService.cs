using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using Dapper;
using MusalaPhoneBook.Entities;
using MusalaPhoneBook.Helpers;

namespace MusalaPhoneBook.Services
{
    public class PhoneBookService:IPhoneBookService
    {
        private string _connectionString;

        public PhoneBookService(string connectionString)
        {
            _connectionString = connectionString;
        }

        public EntityList<Contact> List(int page, int perPage, string filter)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                int offset = (page - 1) * perPage;
                List<Contact> contacts;
                int count;
                if (string.IsNullOrEmpty(filter))
                {
                    contacts = db.Query<Contact>("Select * From  Contacts ORDER BY Name ASC OFFSET @offset ROWS FETCH NEXT @perPage ROWS ONLY", 
                        new { offset= offset, perPage= perPage }).ToList();
                    count = db.ExecuteScalar<int>("SELECT COUNT(*) FROM Contacts");
                }
                else
                {
                    contacts = db.Query<Contact>("Select * From  Contacts WHERE Name LIKE @name ORDER BY Name ASC OFFSET @offset ROWS FETCH NEXT @perPage ROWS ONLY", 
                        new { offset= offset, perPage= perPage, name= "%"+filter+"%" }).ToList();
                    count = db.ExecuteScalar<int>("SELECT COUNT(*) FROM Contacts WHERE Name LIKE @name", 
                        new { name= "%"+filter+"%" });
                }
                return new EntityList<Contact>()
                {
                    Total = count,
                    List = contacts
                };
            }
        }

        public Contact GetByName(string name)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var contact = db.Query<Contact>("Select * From  Contacts " + 
                                          "WHERE Name = @name", new { name }).SingleOrDefault();
                return contact;
            }
        }

        public Contact Create(Contact contact)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                if (string.IsNullOrWhiteSpace(contact.Name))
                    throw new AppException("Name is required");
                if (string.IsNullOrWhiteSpace(contact.PhoneNumber))
                    throw new AppException("Phone number is required");

                var existingContact = db.Query<Contact>("Select * From  Contacts " + 
                                                  "WHERE Name = @Name", new { Name= contact.Name }).Any();
                if (existingContact)
                    throw new AppException("Name \"" + contact.Name + "\" is already taken");
                string number = NormalizePhoneNumber(contact.PhoneNumber);
                if(string.IsNullOrEmpty(number))
                    throw new AppException("Phone number is invalid");

                string query = @"INSERT INTO Contacts (Name, PhoneNumber) Values (@Name, @PhoneNumber);
                               SELECT CAST(SCOPE_IDENTITY() as int)";
                int id = db.Query<int>(query, new {Name = contact.Name, PhoneNumber = number}).Single();
                var created = db.Query<Contact>("Select * From  Contacts " + 
                                                "WHERE Id = @id", new { id }).SingleOrDefault();

                return created;
            }
        }

        public Contact Delete(string name)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var contact = db.Query<Contact>("Select * From  Contacts " + 
                                          "WHERE Name = @name", new { name }).SingleOrDefault();
                if(contact != null)
                    db.Execute("DELETE FROM Contacts WHERE Name=@name", new {name});
                return contact;
            }
        }

        public Contact AddOutgoingCall(string name)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var contact = db.Query<Contact>("Select * From  Contacts " + 
                                                "WHERE Name = @name", new { name }).SingleOrDefault();
                db.Execute("UPDATE Contacts SET OutgoingCalls=@OutgoingCalls WHERE Id=@Id",
                    new {Id=contact.Id, OutgoingCalls= contact.OutgoingCalls + 1});
                return contact;
            }
        }

        public int AddFromlist(string list)
        {
            int created = 0;
            if (!string.IsNullOrEmpty(list))
            {
                var contactTextList = list.Split('\n');
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    foreach (string contactText in contactTextList)
                    {
                        var contactArray = contactText.Split(',');
                        if (contactArray.Length == 2)
                        {
                            if (!string.IsNullOrEmpty(contactArray[0]) && !string.IsNullOrEmpty(contactArray[1]))
                            {
                                string number = NormalizePhoneNumber(contactArray[1]);
                                if (!string.IsNullOrEmpty(number))
                                {
                                    var existingContact = db.Query<Contact>("Select * From  Contacts " +
                                                                            "WHERE Name = @Name",
                                        new {Name = contactArray[0]}).Any();
                                    if (!existingContact)
                                    {
                                        db.Execute(
                                            "INSERT INTO Contacts (Name, PhoneNumber) Values (@Name, @PhoneNumber);",
                                            new {Name = contactArray[0], PhoneNumber = number});
                                        created++;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return created;
        }

        public EntityList<Contact> TopOutgoingCalls(int quantity)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var contacts = db.Query<Contact>("Select TOP(@Quantity) * From  Contacts ORDER BY OutgoingCalls DESC", 
                    new { Quantity= quantity }).ToList();
                return new EntityList<Contact>()
                {
                    Total = quantity,
                    List = contacts
                };
            }
        }

        private string NormalizePhoneNumber(string phoneNumber)
        {
            string result = "";
            phoneNumber = Regex.Replace(phoneNumber, "\\s", "");
            Regex idRegex = new Regex("^(\\+359|00359|0)(87|88|89)[2-9][0-9]{6}$");
            Match match = idRegex.Match(phoneNumber);
            if (match.Success)
            {
                Regex startRegex = new Regex("^(\\+359|00359|0)((87|88|89)[2-9][0-9]{6})$");
                result = startRegex.Replace(phoneNumber, "+359$2");
            }
            return result;
        }
    }
}