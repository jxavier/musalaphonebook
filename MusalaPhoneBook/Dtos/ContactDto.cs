namespace MusalaPhoneBook.Dtos
{
    public class ContactDto
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public int OutgoingCalls { get; set; }
    }
}