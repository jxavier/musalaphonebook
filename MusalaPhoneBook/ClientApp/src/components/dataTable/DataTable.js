import React, {Component} from "react";
import "./dataTable.css";
import {
    Table,
    Pagination,
    PaginationItem,
    PaginationLink,
    Button,
    InputGroup,
    InputGroupAddon,
    Input
} from "reactstrap";
import {Mask} from "../mask/Mask";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFilter} from "@fortawesome/free-solid-svg-icons";
import _ from "lodash";
import {UtilFunctions} from "../../Util/UtilFunctions";

export class DataTable extends Component{
    constructor(props){
        super(props);
        this.state = {
            entities: [],
            count: 0,
            loading: false,
            total: 0,
            perPage: 10,
            currentPage: 1,
            filter: "",
            updateInterval: null,
            width: 0
        };
        this.id = UtilFunctions.generateRandomString(10);
    }
    
    componentDidMount() {
        const self = this;
        let thisComponent = document.getElementById(this.id);
        this.setState({
            width: thisComponent.clientWidth
        });
        this.getData(this.state.currentPage, this.getPerPage(), this.state.filter);
        if(_.isInteger(this.props.updateEvery)){
            this.updateInterval = setInterval(function () {
                self.getData(self.state.currentPage, self.getPerPage(), self.state.filter);
            }, self.props.updateEvery * 1000);
        }
    }
    
    componentWillUnmount() {
        this.clearInterval();
    }
    
    clearInterval(){
        if(this.updateInterval){
            clearInterval(this.updateInterval);
        }
    }

    getPerPage(){
        let perPage = 10;
        if(_.isInteger(this.props.perPage)){
            perPage = this.props.perPage
        }
        return perPage;
    }

    refresh(){
        this.setState({
            currentPage: 1
        });
        this.getData(1, this.getPerPage(), this.state.filter);
    }
    
    goToPage(page){
        this.setState({
            currentPage: page
        });
        this.getData(page, this.getPerPage(), this.state.filter);
    }

    getData(page, perPage, filter){
        if(this.props.getData){
            this.setState({
                loading: true
            });
            this.props.getData(page, perPage, filter).then(data=> {
                this.setState({
                    entities: data.list,
                    total: data.total,
                    loading: false
                });
            }).catch(error=> {
                this.setState({
                    loading: false
                });
            })
        }
    }
    
    onKeyUp(event){
        if(event.which === 13){
            this.filter();
        }
    }
    filter(){
        let filter = document.getElementById("table-filter-field").value;
        this.setState({
            filter: filter,
            currentPage: 1
        });
        this.getData(1, this.getPerPage(), filter);
    }
    
    render(){
        const self = this;
        let pageCount = Math.ceil(this.state.total / this.getPerPage());
        let pagination = "";
        if(pageCount > 1 && this.state.width > 0){
            let pageMax = (this.state.width - (35 * 4)) / 43;
            pageMax = Math.floor(pageMax);
            let pages = [];
            let sidePages = pageMax - 1;
            let leftSidePages = Math.floor(sidePages / 2);
            
            let firstSidePage = this.state.currentPage;
            for (let i = 0; i < leftSidePages; i++) {
                if(firstSidePage === 1)
                    break;
                else{
                    firstSidePage --;
                    pageMax--;
                }
            }

            let rightSidePages = pageMax;
            let lastSidePage = this.state.currentPage;
            for (let i = 0; i < rightSidePages; i++) {
                if(lastSidePage === pageCount)
                    break;
                else{
                    lastSidePage++;
                    pageMax--;
                }
            }
            
            if(pageMax > 0){
                for (let i = 0; i < pageMax; i++) {
                    if(firstSidePage === 1)
                        break;
                    else{
                        firstSidePage --;
                    }
                }
            }
            
            
            for(let i = firstSidePage; i <= lastSidePage; i++){
                pages.push(
                    <PaginationItem active={this.state.currentPage === i} key={i}>
                        <PaginationLink onClick={event=> this.goToPage(i)} href="javascript:">{i}</PaginationLink>
                    </PaginationItem>
                );
            }
            pagination = 
                <Pagination>
                    <PaginationItem disabled={this.state.currentPage === 1}>
                        <PaginationLink onClick={event=> this.goToPage(1)} previous href="javascript:" />
                    </PaginationItem>
                    <PaginationItem disabled={this.state.currentPage === 1}>
                        <PaginationLink onClick={event=> this.goToPage(this.state.currentPage - 1)} href="javascript:" > {"<"} </PaginationLink>
                    </PaginationItem>
                    {pages}
                    <PaginationItem disabled={this.state.currentPage === pageCount}>
                        <PaginationLink onClick={event=> this.goToPage(this.state.currentPage + 1)} href="javascript:" > {">"} </PaginationLink>
                    </PaginationItem>
                    <PaginationItem disabled={this.state.currentPage === pageCount}>
                        <PaginationLink onClick={event=> this.goToPage(pageCount)} next href="javascript:" />
                    </PaginationItem>
                </Pagination>
        }
        let hasActions = false;
        if(this.props.actions){
            if(this.props.actions.length > 0){
                hasActions = true;
            }
        }
        let hasGlobalActions = false;
        if(this.props.globalActions){
            if(this.props.globalActions.length > 0){
                hasGlobalActions = true;
            }
        }
        let globalActions = [];
        if(hasGlobalActions){
            for(let globalAction of this.props.globalActions){
                globalActions.push(<Button key={globalAction.id} color={globalAction.color} onClick={event=> globalAction.action(event)}>{globalAction.icon}</Button>);
            }
        }
        return (
            <div id={this.id} className={"data-table"}>
                <Mask show={this.state.loading}/>
                <div className={"global-actions"}>
                    <div className={"global-actions-buttons"}>
                        {hasGlobalActions?globalActions:""}
                    </div>
                    {this.props.showFilter?<div className={"search-container"}>
                        <InputGroup>
                            <Input id={"table-filter-field"} onKeyUp={this.onKeyUp.bind(this)}/>
                            <InputGroupAddon addonType="append"><Button onClick={this.filter.bind(this)}><FontAwesomeIcon icon={faFilter}/></Button></InputGroupAddon>
                        </InputGroup>
                    </div>:""}
                    
                </div>
                <Table striped>
                    <thead>
                    <tr>
                    {this.props.columns.map(column => {
                        return <th key={column.id}>{column.text}</th>
                    })}
                        {hasActions?<th>Actions</th>:null}
                    </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.entities.length>0?this.state.entities.map(entity => {
                            let actions = [];
                            if(hasActions){
                                for (let action of self.props.actions) {
                                    if(action.show){
                                        if(action.show(entity)){
                                            actions.push(<Button key={action.id} color={action.color} onClick={event=> action.action(event, entity)}>{action.icon}</Button>);
                                        }
                                    }
                                    else{
                                        actions.push(<Button key={action.id} color={action.color} onClick={event=> action.action(event, entity)}>{action.icon}</Button>);
                                    }
                                }
                            }
                            return <tr key={entity.id || entity.name}>{self.props.columns.map(column => {
                                let cellText = entity[column.id];
                                if(column.format){
                                    cellText = column.format(entity);
                                }
                                return <td key={entity.id+"-"+column.id}><span className={"column-name"}>{column.text}</span><span className={"cell-content"}>{cellText}</span></td>
                            })}
                                {hasActions?<td className={"action-cell"}>{actions}</td>:null}
                            </tr>
                        }):<tr><td style={{textAlign: "center"}} colSpan={hasActions?self.props.columns.length+1:self.props.columns.length}>Data not found</td></tr>
                    }
                    </tbody>
                </Table>
                {pagination}
            </div>
        );
    }
}