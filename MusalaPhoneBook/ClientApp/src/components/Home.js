import React, {Component} from 'react';
import {DataTable} from "./dataTable/DataTable";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { faTrash, faUpload, faPlus, faPhone } from '@fortawesome/free-solid-svg-icons';
import {Modal, ModalHeader, ModalBody, Alert, Button, Table } from "reactstrap";
import {ContactForm} from "./ContactForm";
import {Mask} from "./mask/Mask";
import {UploadContactForm} from "./UploadContactForm";

export class Home extends Component {
    static displayName = Home.name;
    constructor(props){
        super(props);
        this.state = {
            showForm: false,
            currentContact: null,
            deleteModalOpen: false,
            createModalOpen: false,
            showMask: false,
            uploadFormOpen: false,
            topCallsModalOpen: false,
            topContacts: []
        };
        const self = this;
        this.columns = [
            {
                id: "name",
                text: "Name"
            },
            {
                id: "phoneNumber",
                text: "Phone number",
                format:function (contact) {
                    return <a onClick={event => self.addCall(contact)} href={"tel:"+contact.phoneNumber}>{contact.phoneNumber}</a>
                }
            },
        ];
        this.globalActions = [
            {
                id: "create",
                icon: <FontAwesomeIcon icon={faPlus}/>,
                action: this.handleCreate.bind(this),
                color: "success"
            },
            {
                id: "upload",
                icon: <FontAwesomeIcon icon={faUpload}/>,
                action: this.toggleUploadModal.bind(this),
                color: "primary"
            },
            {
                id: "topCalls",
                icon: <FontAwesomeIcon icon={faPhone}/>,
                action: this.getTopCalls.bind(this),
                color: "info"
            },
        ];

        this.actions = [
            {
                id: "delete",
                icon: <FontAwesomeIcon icon={faTrash}/>,
                action: this.handleDelete.bind(this),
                color: "danger"
            }
        ];
    }

    handleDelete(event, contact){
        this.setState({
            currentContact: contact
        });
        this.toggleDeleteModal();
    }

    handleCreate(event){
        this.toggleCreateModal();
    }

    toggleCreateModal() {
        this.setState(prevState => ({
            createModalOpen: !prevState.createModalOpen
        }));
    }

    toggleTopCallModal() {
        this.setState(prevState => ({
            topCallsModalOpen: !prevState.topCallsModalOpen
        }));
    }
    
    getTopCalls(){
        const self = this;
        this.props.entityManager.phoneBookService.topOutgoingCalls(5).then(contactList => {
            self.setState({
                topContacts: contactList.list,
                topCallsModalOpen: true
            })
        });
    }

    getContacts(page, perPage, filter){
        return this.props.entityManager.phoneBookService.list(page, perPage, filter);
    }

    createContact(contact){
        this.setState({
            createModalOpen: false,
            showMask: true
        });
        const self = this;
        this.props.entityManager.phoneBookService.create(contact).then(contact => {
            this.setState({
                showMask: false
            });
            self.contactRef.refresh();
        }).catch(error => {
            this.setState({
                showMask: false
            });
        })
    }

    toggleDeleteModal(){
        this.setState(prevState => ({
            deleteModalOpen: !prevState.deleteModalOpen
        }));
    }

    toggleUploadModal(){
        this.setState(prevState => ({
            uploadFormOpen: !prevState.uploadFormOpen
        }));
    }

    deleteContact(){
        this.setState({
            deleteModalOpen: false,
            showMask: true
        });
        const self = this;
        this.props.entityManager.phoneBookService.delete(this.state.currentContact).then(contact => {
            this.setState({
                showMask: false
            });
            self.contactRef.refresh();
        }).catch(error => {
            this.setState({
                showMask: false
            });
        })
    }
    
    addCall(contact){
        this.props.entityManager.phoneBookService.addCall(contact);
    }
    
    uploadContacts(file){
        this.setState({
            uploadFormOpen: false,
            showMask: true
        });
        const self = this;
        this.props.entityManager.phoneBookService.uploadContactlist(file).then(resp => {
            this.setState({
                showMask: false
            });
            self.contactRef.refresh();
        }).catch(error => {
            this.setState({
                showMask: false
            });
        })
    }

    render() {
        return (
            <div>
                <h1>Contacts</h1>
                <Mask show={this.state.showMask}/>
                <DataTable columns={this.columns}
                           getData={this.getContacts.bind(this)}
                           perPage={10}
                           ref={node => {
                               this.contactRef = node;
                           }}
                           showFilter
                           globalActions={this.globalActions}
                           actions={this.actions}
                />
                <Modal isOpen={this.state.createModalOpen} toggle={this.toggleCreateModal.bind(this)}>
                    <ModalHeader toggle={this.toggleCreateModal.bind(this)}>Add contact</ModalHeader>
                    <ModalBody>
                        <ContactForm create={true} submit={this.createContact.bind(this)}/>
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.deleteModalOpen} toggle={this.toggleDeleteModal.bind(this)}>
                    <ModalHeader toggle={this.toggleDeleteModal.bind(this)}>Delete contact</ModalHeader>
                    <ModalBody>
                        <Alert color="danger">
                            Are you sure you want to delete this contact?
                        </Alert>
                        <Button color={"danger"} onClick={this.deleteContact.bind(this)}>Delete</Button>
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.uploadFormOpen} toggle={this.toggleUploadModal.bind(this)}>
                    <ModalHeader toggle={this.toggleUploadModal.bind(this)}>Upload contacts</ModalHeader>
                    <ModalBody>
                        <UploadContactForm submit={this.uploadContacts.bind(this)}/>
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.topCallsModalOpen} toggle={this.toggleTopCallModal.bind(this)}>
                    <ModalHeader toggle={this.toggleTopCallModal.bind(this)}>Top contacts by outgoing calls</ModalHeader>
                    <ModalBody>
                        <Table>
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Phone number</th>
                                <th>Outgoing calls</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.state.topContacts.length === 0?<tr><td style={{textAlign: "center"}} colSpan={3}>No data found</td></tr>:null}
                            {this.state.topContacts.map(contact => {
                                return <tr>
                                    <td>{contact.name}</td>
                                    <td>{contact.phoneNumber}</td>
                                    <td>{contact.outgoingCalls}</td>
                                </tr>
                            })}
                            </tbody>
                        </Table>
                    </ModalBody>
                </Modal>
            </div>
        );
    }
}
