export class Notifier {
    constructor(notifier){
        this.notifier = notifier;
    }

    showNotification(message, type, duration){
        this.notifier.current.addNotification({
            message: message,
            type: type,
            insert: "top",
            container: "top-center",
            animationIn: ["animated", "fadeIn"],
            animationOut: ["animated", "fadeOut"],
            dismiss: { duration: duration * 1000 },
            dismissable: { click: true }
        });
    }

    showSuccessNotification(message){
        this.showNotification(message, "success", 20);
    }

    showFailedNotification(message){
        this.showNotification(message, "danger", 20);
    }
}