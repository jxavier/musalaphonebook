import React, {Component} from "react";
import "./mask.css";

export class Mask extends Component{
    render(){
        if(this.props.show){
            return (
                <div className={"mask-element"}>

                </div>
            );
        }
        return "";
    }
}