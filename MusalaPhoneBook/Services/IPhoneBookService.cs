using MusalaPhoneBook.Entities;

namespace MusalaPhoneBook.Services
{
    public interface IPhoneBookService
    {
        EntityList<Contact> List(int page, int perPage, string filter);
        Contact GetByName(string name);
        Contact Create(Contact contact);
        Contact Delete(string name);
        Contact AddOutgoingCall(string name);
        int AddFromlist(string list);
        EntityList<Contact> TopOutgoingCalls(int quantity);
    }
}