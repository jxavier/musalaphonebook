export class EntityService {

    setNotification(notifier){
        this.notifier = notifier;
    }
    
    request(requestParams){
        const self = this;
        return new Promise(function (resolve, reject) {
            let fetchOptions = {
                method: requestParams.method,
                headers: {}
            };
            if(requestParams.method === "POST" || requestParams.method === "PUT"){
                if(requestParams.body){
                    if(requestParams.body instanceof File){
                        console.log("entro", requestParams.body);
                        let formData  = new FormData();
                        formData.append("file", requestParams.body);
                        fetchOptions.body = formData;
                    }
                    else{
                        fetchOptions.headers["Content-Type"] = "application/json";
                        fetchOptions.body = JSON.stringify(requestParams.body);
                    }
                }
            }
            fetch(requestParams.url, fetchOptions)
                .then(response => {
                    response.json().then(data => {
                        if(response.status === 200){
                            resolve(data);
                        }
                        else{
                            let message = "Unexpected error";
                            if(data.message)
                                message = data.message;
                            self.notifier.showFailedNotification(message);
                            console.error("Ha ocurrido un error", message);
                            reject(response);
                        }
                    }).catch(error => {
                        self.notifier.showFailedNotification("Unexpected error");
                        console.error("Unexpected error");
                        reject(error);
                    });
                })
                .catch(error => {
                    self.notifier.showFailedNotification("Unexpected error");
                    console.error("Unexpected error");
                    reject(error);
                });
        })
    }
}