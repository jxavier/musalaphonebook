import {PhoneBookService} from "./PhoneBookService";
import {Notifier} from "../Util/Notifier";

export class EntityManager{
    constructor(){
        this.phoneBookService = new PhoneBookService();
    }
    
    setNotificationComponent(notifier){
        this.notifier = new Notifier(notifier);
        this.phoneBookService.setNotification(this.notifier);
    }
}