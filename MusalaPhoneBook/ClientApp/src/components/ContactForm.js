/**
 * Created by Javier Domínguez Pérez<xavier.uci@gmail.com> on 5/13/2019
 */
import React, {Component} from "react";
import { Form, FormGroup, Label, Input, Button, FormFeedback } from 'reactstrap';
import _ from "lodash";

export class ContactForm extends Component{
    constructor(props){
        super(props);
        this.state = {
            nameValid: true,
            phoneValid: false
        }
    }
    componentDidMount() {
        if(this.props.contact){
            document.getElementById("name-field").value = this.props.contact.name;
            document.getElementById("phone-field").value = this.props.contact.phoneNumber;
        }
    }

    handleSubmit(event){
        event.preventDefault();
        const formData = new FormData(event.target);
        let data = {};
        formData.forEach((value, key)=> {
            data[key] = value;
        });
        let nameValid = !_.isEmpty(data.name);
        let phoneValid = !_.isEmpty(data.phoneNumber);
        this.setState({
            nameValid: nameValid,
            phoneValid: phoneValid,
        });
        console.log("data", data);
        if(nameValid && phoneValid){
            console.log("entro");
            this.props.submit(data);
        }
    }
    handleOnChange(field, event){
        switch (field) {
            case "name":
                let nameValid = !_.isEmpty(event.target.value);
                this.setState({
                    nameValid: nameValid
                });
                break;
            case "phoneNumber":
                let phoneValid = !_.isEmpty(event.target.value);
                this.setState({
                    phoneValid: phoneValid
                });
                break;
        }
    }
    render(){
        return (
            <div>
                <Form onSubmit={this.handleSubmit.bind(this)}>
                    <FormGroup>
                        <Label>Name</Label>
                        <Input id={"name-field"} onChange={this.handleOnChange.bind(this, "name")} invalid={!this.state.nameValid} type={"text"} name={"name"}/>
                        {!this.state.nameValid?<FormFeedback>Required</FormFeedback>:""}
                    </FormGroup>
                    <FormGroup>
                        <Label>Phone</Label>
                        <Input id={"phone-field"} onChange={this.handleOnChange.bind(this, "phoneNumber")} invalid={!this.state.nameValid} type={"text"} name={"phoneNumber"}/>
                        {!this.state.phoneValid?<FormFeedback>Required</FormFeedback>:""}
                    </FormGroup>
                    <Button color={"primary"}>Save</Button>
                </Form>
            </div>
        );
    }
}