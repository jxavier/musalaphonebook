import React, {Component} from 'react';
import {Route} from 'react-router';
import {Layout} from './components/Layout';
import {Home} from './components/Home';
import {EntityManager} from "./services/EntityManager";
import ReactNotification from "react-notifications-component";
import "react-notifications-component/dist/theme.css";

export default class App extends Component {
    static displayName = App.name;

    constructor(props) {
        super(props);
        this.entityManager = new EntityManager();
        this.notificationDOMRef = React.createRef();
    }

    componentDidMount() {
        this.entityManager.setNotificationComponent(this.notificationDOMRef);
    }

    render() {
        return (
            <Layout>
                <ReactNotification ref={this.notificationDOMRef} />
                <Route exact path='/' render={(props)=><Home {...props} entityManager={this.entityManager}/>}/>
            </Layout>
        );
    }
}
