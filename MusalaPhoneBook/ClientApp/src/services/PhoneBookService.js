/**
 * Created by Javier Domínguez Pérez<xavier.uci@gmail.com> on 5/12/2019
 */
import {EntityService} from "./EntityService";

export class PhoneBookService extends EntityService {
    list(page, perPage, filter) {
        return this.request({
            url: 'api/PhoneBook?page=' + page + "&perPage=" + perPage + "&filter=" + filter,
            method: "GET"
        });
    }

    create(contact) {
        return this.request({
            url: 'api/PhoneBook',
            method: "POST",
            body: contact
        });
    }

    delete(contact){
        return this.request({
            url: 'api/PhoneBook/'+contact.name,
            method: "DELETE"
        });
    }
    
    addCall(contact){
        return this.request({
            url: 'api/PhoneBook/'+contact.name+'/addcall',
            method: "PUT"
        });
    }
    
    uploadContactlist(file){
        return this.request({
            url: 'api/PhoneBook/addfromlist',
            method: "POST",
            body: file
        });
    }

    topOutgoingCalls(quantity) {
        return this.request({
            url: 'api/PhoneBook/topcalls?quantity=' + quantity,
            method: "GET"
        });
    }
}