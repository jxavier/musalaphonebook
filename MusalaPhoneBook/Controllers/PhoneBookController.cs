using System.Collections.Generic;
using System.IO;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MusalaPhoneBook.Dtos;
using MusalaPhoneBook.Entities;
using MusalaPhoneBook.Helpers;
using MusalaPhoneBook.Services;

namespace MusalaPhoneBook.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PhoneBookController:ControllerBase
    {
        private IPhoneBookService _phoneBookService;
        private IMapper _mapper;

        public PhoneBookController(IPhoneBookService phoneBookService, IMapper mapper)
        {
            _phoneBookService = phoneBookService;
            _mapper = mapper;
        }
        
        [HttpGet]
        public IActionResult List(int page, int perPage, string filter)
        {
            var contactList =  _phoneBookService.List(page, perPage, filter);
            var contactDtos = _mapper.Map<IList<ContactDto>>(contactList.List);
            return Ok(new {Total = contactList.Total, List = contactDtos});
        }
        
        [HttpPost]
        public IActionResult Create([FromBody]ContactDto contactDto)
        {
            // map dto to entity
            var contact = new Contact()
            {
                Name = contactDto.Name,
                PhoneNumber = contactDto.PhoneNumber
            };

            try 
            {
                // save 
                var contactCreated = _phoneBookService.Create(contact);
                return Ok(new
                {
                    Name = contact.Name,
                    PhoneNumber = contact.PhoneNumber,
                    OutgoingCalls = contact.OutgoingCalls
                });
            } 
            catch(AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
        
        [HttpGet("{name}")]
        public IActionResult GetById(string name)
        {
            var contact =  _phoneBookService.GetByName(name);
            var contactDto = _mapper.Map<ContactDto>(contact);
            return Ok(contactDto);
        }
        
        [HttpDelete("{name}")]
        public IActionResult Delete(string name)
        {
            var contact = _phoneBookService.Delete(name);
            return Ok(new
            {
                Name = contact.Name,
                PhoneNumber = contact.PhoneNumber,
                OutgoingCalls = contact.OutgoingCalls
            });
        }
        
        [HttpPut("{name}/addcall")]
        public IActionResult AddCall(string name)
        {
            var contact =  _phoneBookService.AddOutgoingCall(name);
            return Ok(new
            {
                Name = contact.Name,
                PhoneNumber = contact.PhoneNumber,
                OutgoingCalls = contact.OutgoingCalls
            });
        }
        
        [HttpPost("addfromlist")]
        public IActionResult AddFromlist(IFormFile file)
        {
            var reader = new StreamReader(file.OpenReadStream());
            string list = reader.ReadToEnd();
            var result = _phoneBookService.AddFromlist(list);
            return Ok(new {Created= result});
        }
        
        [HttpGet("topcalls")]
        public IActionResult TopOutgoingCalls(int quantity)
        {
            var contactList =  _phoneBookService.TopOutgoingCalls(quantity);
            var contactDtos = _mapper.Map<IList<ContactDto>>(contactList.List);
            return Ok(new {Total = contactList.Total, List = contactDtos});
        }
    }
}