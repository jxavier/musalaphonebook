/**
 * Created by Javier Domínguez Pérez<xavier.uci@gmail.com> on 5/13/2019
 */
import React, {Component} from "react";
import {Button, Form, FormGroup, Input, Label} from "reactstrap";

export class UploadContactForm extends Component {
    constructor(props){
        super(props);
        this.state = {
            file: null
        };
    }
    handleOnChange(event){
        this.setState({
            file: event.target.files[0]
        });
    }
    handleSubmit(event){
        event.preventDefault();
        console.log("file", this.state.file);
        if(this.state.file instanceof File)
            this.props.submit(this.state.file);
    }
    render() {
        return (
            <div>
                <Form onSubmit={this.handleSubmit.bind(this)}>
                    <FormGroup>
                        <Label>Contact list</Label>
                        <Input id={"file-field"} onChange={this.handleOnChange.bind(this)} type={"file"} name={"fileField"}/>
                    </FormGroup>
                    <Button color={"primary"}>Save</Button>
                </Form>
            </div>
        );
    }
}